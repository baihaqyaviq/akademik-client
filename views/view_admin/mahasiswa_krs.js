define([
	"views/modules/mhs_search",
    "views/modules/grid_mk_kelas",
    "views/modules/handle_processing"
], function(search, grid_mk_kelas, handle){

var grd_krs = {
	view:"datatable",
	id:"grd_krs",
    footer:true,
	columns:[
		{ id: "trash", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o text_danger'></span>",},
		{ id: "id_smt", header:"smt", width:80,  },
		{ id: "kode_mk", header:"kode mk", width:100},
		{ id: "nm_mk", header:"nama mk", width:300, footer:{text:"Total SKS:", colspan:2} },
		{ id: "nm_kls", header:"kls", width:80, editor:"text"},
		{ id: "sks_mk", header:"sks", width:80, footer:{content:"summColumn"}}
	],
	onClick:{
		webix_icon:function(e,id,node){
            handle.onBeforeDelete(e,id,node,'grd_krs');
		}
	},
	select:"row",
    dataFeed:function(id){
        this.load("laravel->/api/mahasiswa/"+id+"/krs");
    },
    save: handle.onSave("laravel->/api/mahasiswa/krs"),
    ready: handle.emptydata,
    on: {
        onAfterAdd : function(id, index){
            handle.onAfterAdd(id, index, 'bg_info', 'grd_mk_kls')
        },
        onBeforeLoad: handle.onBeforeLoad,
        onAfterLoad:function(){
            handle.onAfterLoad(this);

            $$("lbl_krs").define("template", "Jml Matakuliah yang diambil: "+ this.count() );
            $$("lbl_krs").refresh();
        }
    }
};

var btn_add ={
    paddingX:5,
    paddingY:5,
    height:40,
    cols:[
        {id:"lbl_krs", view :"label", width:250 },
        {},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "Add Matakuliah", width: 180, click:function(obj){
            var id = $$("listmsmhs").getSelectedId();
            var mhs = $$("listmsmhs").getSelectedItem();

            if (id) {
                webix.$$("win_krs_mhs").show();
            }else{
                webix.message({ type:"error", text:"Please select one", expire:3000});
            }
        }},
        { view: "button", type: "iconButton", icon: "print", label: "KRS", width: 100, click:function(){
            var listmsmhs = $$("listmsmhs").getSelectedId();
            var values = {id_reg_pd:listmsmhs.id};

            if (listmsmhs) {
                webix.send("laravel->/preview/krs",values, 'POST', '_balnk');
            }else{
                webix.message({ type:"error", text:"Please select one", expire:3000});
            }
        }}
    ]
};

var btn_add_krs ={
    paddingX:10,
    paddingY:10,
    height:50,
    cols:[
        {},
        { view: "button", css:"button_danger", label: "Cancel", width: 120, click:function(obj){
             this.getTopParentView().hide();
        }},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "Submit", width: 120, click:function(obj){
            var items     = $$("grd_mk_kls").getSelectedItem();
            var listmsmhs = $$("listmsmhs").getSelectedId();

            if (items){
                if (Array.isArray(items)) {
                    for (var i=0; i< items.length; i++){
                        items[i].id_reg_pd = listmsmhs.id;
                        $$("grd_krs").add(items[i]);
                    }
                }else{
                    items.id_reg_pd = listmsmhs.id;
                    $$("grd_krs").add(items);
                }

                this.getTopParentView().hide();
            }else{
                webix.message({ type:"error", text:"Please select one", expire:3000});
            }
        }}
    ]
};

var ui_scheme = {
    rows:[
    {
    	type: "material",
    	cols:[
    		search,
    		{
                gravity: 2.2,
                rows:[
    				grd_krs,
                    btn_add
                ]
            }
    	]
    }
    ],
};

var ui_window = {
    view:"window",
    id:"win_krs_mhs",
    modal:true,
    position:"center",
    width:700,
    height:500,
    head:{
        view:"toolbar", margin:-4, cols:[
            { view:"label", id:"lbl_win_krs_mhs", label: "DAFTAR MATAKULIAH" },
            { view:"icon", icon:"question-circle", click:"webix.message('About pressed')"},
            { view:"icon", icon:"times-circle", click:function(){
                this.getTopParentView().hide();
            }}
        ]
    },
    body: {
        rows:[ grid_mk_kelas, btn_add_krs ]
    }
};

return {
    $ui:ui_scheme,
    $windows:[ ui_window ],
    $oninit: function(view, $scope){

        //if type datatable, harus seperti ini
        $$("grd_krs").bind($$("listmsmhs"), 'id');

        //fix me: tambahkan data id_sms (id prodi) pada listmsmhs
        $$("grd_mk_kls").bind( $$("listmsmhs"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/prodi/"+data.id_sms+"/matakuliah");
            }
        });

        /*var dp = new webix.DataProcessor({
            updateFromResponse:true,
            autoupdate:true,
            master: $$("grd_krs"),
            url: "laravel->/api/mahasiswa/krs",
            on:{
                onAfterSync: handle.onAfterSync,
                onAfterSaveError: handle.onAfterSaveError
            }
        });*/

	},
    $ondestroy:function(){
        $$("grd_krs").destructor();
    }
};
});
