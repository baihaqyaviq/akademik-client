define([
	"views/modules/kurikulum_search",
    "views/forms/frm_kursem",
    "views/modules/grid_mata_kuliah",
    "views/modules/handle_processing"
], function(search, form, grid_mata_kuliah, handle){


var grd_kursem = {
	view:"datatable",
	id:"grd_kursem",
	columns:[
        { id:"trash", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon trash fa-trash-o text_danger'></span>"},
        { id:"smt",width:80, editor:"select", options:[1,2,3,4,5,6,7,8], header:[{
            content:"textFilter", placeholder:"SMT",  compare:handle.startCompare
        }]},
        { id:"a_wajib",  checkValue:'1', uncheckValue:'0', template:"{common.checkbox()}"},
        { id:"kode_mk", width:100, header:[{
            content:"textFilter", placeholder:"Kode MK", compare:handle.startCompare
        }]},
        { id:"nm_mk", width:250, header:[{
            content:"textFilter", placeholder:"Nama MK", compare:handle.startCompare
        }]},
        { id:"sks_mk", header: "sks_mk", width:80, editor:"text"},
        { id:"sks_tm", header: "sks_tm", width:80, editor:"text"},
        { id:"sks_prak", header: "sks_prak", width:80, editor:"text"},
        { id:"sks_prak_lap", header: "sks_prak_lap", width:80, editor:"text"},
        { id:"sks_sim", header: "sks_sim", width:80, editor:"text"},
	],
	select:"row", editable:true,
    dataFeed:function(id){
        this.load("laravel->/api/kurikulum/"+id+"/matakuliah");
    },
    onClick:{
        trash:function(e,id,node){
            webix.confirm({
                text:"The data will be deleted.<br/> Are you sure?", ok:"Yes", cancel:"Cancel",
                callback:function(res){
                    if(res){
                        $$("grd_kursem").remove(id);
                    }
                }
            });
        },
    },
    on:{
        onAfterLoad:function(){
            $$("lbl_mk").define("template", "Jml Mata Kuliah: "+this.count() );
            $$('lbl_mk').refresh();
        }
    }
};

var btn_add_list ={
    paddingX:5,
    paddingY:5,
    height:40,
    cols:[
        {},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "New", width: 100, click:function(obj){
            webix.$$("win_list").show();
        }}
    ]
};

var btn_add_detail ={
    paddingX:5,
    paddingY:5,
    height:40,
    cols:[
        {id:"lbl_mk", view :"label", width:200 },
        {},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "ADD MATAKULIAH", width: 180, click:function(obj){
            var id = $$("list_kurikulum").getSelectedId();
            if (id) {
                webix.$$("win_detail").show();
            }else{
                webix.message({ type:"error", text:"Please select one", expire:3000});
            }
        }}
    ]
};

var btn_submit_list ={
    paddingX:10,
    paddingY:10,
    height:50,
    cols:[
        {},
        { view: "button", css:"button_danger", label: "Cancel", width: 120, click:function(obj){
             this.getTopParentView().hide();
        }},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "Submit", width: 120, click:function(obj){
            var form = $$('frm_kursem');
            // var values    = form.getValues();

            if(form.isDirty()){
                if(!form.validate())
                    return false;

                form.save();
                this.getTopParentView().hide(); //hiteme window
            };
        }}
    ]
};

var btn_submit_mk_smt ={
    paddingX:10,
    paddingY:10,
    height:50,
    cols:[
        {},
        { view: "button", css:"button_danger", label: "Cancel", width: 120, click:function(obj){
             this.getTopParentView().hide();
        }},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "Submit", width: 120, click:function(obj){
            var items          = $$("grd_mata_kuliah").getSelectedItem();
            var list_kurikulum = $$("list_kurikulum").getSelectedId();

            if (items){
                if (Array.isArray(items)) {
                    for (var i=0; i< items.length; i++){
                        items[i].id_kurikulum_sp = list_kurikulum.id;
                        items[i].id_mk = items[i].id;
                        $$("grd_kursem").add(items[i]);
                    }
                }else{
                    items.id_mk = items.id;
                    items.id_kurikulum_sp = list_kurikulum.id;
                    $$("grd_kursem").add(items);
                }

                this.getTopParentView().hide();
            }else{
                webix.message({ type:"error", text:"Please select one", expire:3000});
            }
        }}
    ]
};

var ui_scheme = {
	type: "line",
	id:"ui_kursem",
    rows:[
    {
    	margin:10,
    	type: "material",
        	cols:[
            {
        		rows:[
                    search,
                    btn_add_list
                ]
            },
            {
                gravity: 2.2,
                rows:[
                    grd_kursem,
                    btn_add_detail
                ]
            }
    	]
    }
    ],
};

var ui_window = {
    view:"window",
    id:"win_list",
    modal:true,
    position:"center",
    width:700,
    height:500,
    head:{
        view:"toolbar", margin:-4, cols:[
            { view:"label", id:"lbl_win_list", label: "KURIKULUM SEMESTER" },
            { view:"icon", icon:"question-circle", click:"webix.message('About pressed')"},
            { view:"icon", icon:"times-circle", click:function(){
                this.getTopParentView().hide();
            }}
        ]
    },
    body: {
        rows:[form, btn_submit_list]
    }
};

var ui_window2 = {
    view:"window",
    id:"win_detail",
    modal:true,
    position:"center",
    width:700,
    height:500,
    head:{
        view:"toolbar", margin:-4, cols:[
            { view:"label", id:"lbl_win_detail", label: "MATAKULIAH KURIKULUM" },
            { view:"icon", icon:"question-circle", click:"webix.message('About pressed')"},
            { view:"icon", icon:"times-circle", click:function(){
                this.getTopParentView().hide();
            }}
        ]
    },
    body: {
       rows:[grid_mata_kuliah, btn_submit_mk_smt]
    }
};

return {
    $ui: ui_scheme,
    $windows:[ui_window, ui_window2],
    $oninit: function(){

        $$("grd_kursem").bind($$("list_kurikulum"), 'id');


        //binding with grid but grid is dataFeed:true
        $$("grd_mata_kuliah").bind( $$("list_kurikulum"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/"+data.id_sms+"/matakuliah");
            }
        });

        /*$$("frm_kursem").bind( $$("list_kurikulum"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/kurikulumsmt"+data.id_sms);
            }
        });*/

        var dp = new webix.DataProcessor({
            updateFromResponse:true,
            autoupdate:true,
            master: $$("list_kurikulum"),
            url: "laravel->/api/kurikulum",
            // on:{
            //     onAfterSync: handle.onAfterSync,
            //     onAfterSaveError: handle.onAfterSaveError
            // }
        });

        var dp1 = new webix.DataProcessor({
            updateFromResponse:true,
            autoupdate:true,
            master: $$("grd_kursem"),
            url: "laravel->/api/kurikulumsmt",
            // on:{
            //     onAfterSync: handle.onAfterSync,
            //     onAfterSaveError: handle.onAfterSaveError
            // }
        });
	},

};
});
