define([
	"views/forms/frm_mhs_biodata",
	"views/modules/dataProcessing",
	"views/modules/dataProgressBar",
],function(forms, handleProcessing, notifidata){

var grd_mahasiswa = {
	view:"datatable",
	id:"grd_mahasiswa",
	columns:[
		{ id: "nipd", width:200, header: ["NIM", {content: "serverFilter"}], sort: 'server'},
		{ id: "nm_pd", width:200, header: ["Nama Mahasiswa", {content: "serverFilter"}], sort: 'server'},
		{ id: "jk", width:200, header: ["Jenis Kelamin", {content: "serverFilter"}], sort: 'server'},
		{ id: "tmpt_lahir", width:200, header: ["Tempat Lahir", {content: "serverFilter"}], sort: 'server'},
		{ id: "tgl_lahir", width:200, header: ["Tanggal Lahir", {content: "serverFilter"}], sort: 'server'},
		{ id: "nm_stat_mhs", width:200, header: ["Status Mahasiswa", {content: "serverFilter"}], sort: 'server'}
	],
	datafetch:10,
	pager: 'grd_mahasiswa_pager',
	select:"row",
	url: "laravel->/api/mahasiswa",
	ready: notifidata.emptydata,
	on: notifidata.progressbar
};

var pager = {
	view: 'pager',
	id: 'grd_mahasiswa_pager',
	size:10,
	group:5,
	paddingX:5,
	paddingY:5,
};

var btn_save ={
	paddingX:5,
	paddingY:5,
	height:45,
	cols:[
		{},
		{ view: "button", css:"button_raised", label: "save", width: 130, click:function(obj){
			var form = $$('formMsmhs');
			if(form.isDirty()){
				if(!form.validate())
					return false;

				// form.save();
				$$("grd_mahasiswa").parse(form.getValues());
				$$("grd_mahasiswa").add(form.getValues());
				// webix.ajax().post("./mahasiswa/detail?action=add", form.getValues());
			};
		}}
	]
};

var ui_scheme = {
	type: "material",
	rows:[
		{
			css:"bg_clean",
	    	cols:[
		    	{
		    		view:"segmented",
		    		id:'tabbar',
		    		value: 'lst_mhs',
			    	multiview:true,
			    	optionWidth:141,
			    	padding: 5,
			    	options: [
				    	{ value: 'List Mahasiswa', id: 'lst_mhs'},
				    	{ value: 'Add new', id: 'frm_mhs'}
			    	]
		    	}
	    	]
		},
		{
			id:"multiview_mhs",
	    	cells:[
		    	{
		    		id:"lst_mhs",
		    		rows: [ grd_mahasiswa, pager ]
		    	},
		    	{
		    		id:"frm_mhs",
		    		rows:[forms, btn_save]
		    	}
	    	]

		}
	]
};

return {
    $ui: ui_scheme,
    $oninit:function(){
    	// $$("formMsmhs").bind( $$("grd_mahasiswa") );
    	// $$("formMsmhs").define("dataFeed","./mahasiswa/detail");

    	var dp = new webix.DataProcessor({
		    updateFromResponse:true,
		    autoupdate:true,
		    master: $$("grd_mahasiswa"),
		    url: "laravel->/api/mahasiswa",
		    on: handleProcessing
		});

		/*webix.UIManager.addHotKey("enter", function(view){
			var pos = view.getSelectedId();
			view.edit(pos);
		}, grd_mahasiswa);*/
    }
};
});
