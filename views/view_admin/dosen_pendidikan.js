define([
	"views/modules/dosen_search",
	"views/forms/frm_dosen_riwpend",
	"views/modules/dataProcessing"
],function(search, form, handleProcessing){

var grd_riwpend = {
	view:"datatable",
	id:"grd_riwpend",
	columns:[
		{ id:"trash", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon trash fa-trash-o text_danger'></span>"},
		{ id:"edit", header:"&nbsp;", width:35, template:"<span  style=' cursor:pointer;' class='webix_icon edit fa-pencil'></span>"},
		{ id:"nm_jenj_didik", header:"nm_jenj_didik", width: 100},
        { id:"nm_pt", header:"nm_pt", width: 100},
        { id:"bidang_studi", header:"bidang_studi", width: 100},
        { id:"fakultas", header:"fakultas", width: 100},
        { id:"gelar", header:"gelar", width: 100},
        { id:"ipk_lulus", header:"ipk_lulus", width: 100},
        { id:"sks_lulus", header:"sks_lulus", width: 100},
        { id:"thn_lulus", header:"thn_lulus", width: 100}
	],
	select:"row",
	datafetch:30,
	// dataFeed : "./riwpend/data",
	onClick:{
		edit:function(e,id,node){
			webix.$$("win_riwpend").show();
		},
		trash:function(e,id,node){
			webix.confirm({
				text:"The data will be deleted.<br/> Are you sure?", ok:"Yes", cancel:"Cancel",
				callback:function(res){
					if(res){
						$$("grd_riwpend").remove(id);
					}
				}
			});
		},
	}
};

var btn_add ={
	paddingX:5,
	paddingY:5,
	height:40,
	cols:[
		{},
		{ view: "button", type: "iconButton", icon: "plus", css:"button_success", label: "Add New", width: 130, click:function(obj){
			var id = $$("listdosen").getSelectedId();
			if (id) {
				webix.$$("win_riwpend").show();
			}else{
				webix.message({ type:"error", text:"Please select one", expire:3000});
			}
		}}
	]
};

var btn_submit ={
    height:50,
    padding:10,
    cols:[
        {},
        { view: "button", css:"button_danger", label: "Cancel", width: 120, click:function(obj){
             this.getTopParentView().hide();
        }},
        { view: "button", type: "iconButton", icon: "plus", css:"button_success ", label: "Submit", width: 120, click:function(obj){
			var form      = $$('frm_dosen_riwpend');
			var dosen     = $$("listdosen").getSelectedId();
			var values    = form.getValues();
			values.id_ptk = dosen.id;

			if(form.isDirty()){
				if(!form.validate())
					return false;


				form.setValues( values );
				form.save();
                this.getTopParentView().hide(); //hiteme window
			};
        }}
    ]
};

var ui_scheme = {
	type: "line",
	id:"ui_riwpend",
	rows:[
	{
		margin:10,
		type: "material",
		cols:[
			search,
			{
				gravity: 2.2,
				rows:[
					grd_riwpend,
					btn_add
				]
			}
		]
	}
	]
};


var ui_window = {
    view:"window",
    id:"win_riwpend",
    modal:true,
    position:"center",
    width:700,
    height:500,
    head:{
        view:"toolbar", margin:-4, cols:[
            { view:"label", id:"lbl_win_riwpend", label: "FORM RIWAYAT PENDIDIKAN" },
            { view:"icon", icon:"question-circle", click:"webix.message('About pressed')"},
            { view:"icon", icon:"times-circle", click:function(){
                this.getTopParentView().hide();
            }}
        ]
    },
    body: {
        rows:[form, btn_submit]
    }
};

return {
    $ui: ui_scheme,
    $windows:[ui_window],
    $oninit: function(){
        $$("grd_riwpend").bind( $$("listdosen"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/dosen"+data.id+"/pendidikan");
            }
        });

        $$("frm_dosen_riwpend").bind( $$("listdosen"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/dosen/pendidikan");
            }
        });

		var dp = new webix.DataProcessor({
            updateFromResponse:true,
            autoupdate:true,
            master: $$("grd_riwpend"),
            url: "laravel->/api/dosen/pendidikan",
            on: handleProcessing
        });
	},
    $ondestroy:function(){
        $$("grd_riwpend").destructor();
    }
};

});
