define([
	"views/modules/sample/dashline",
	"views/modules/sample/visitors",
	"views/modules/sample/orders",
	"views/modules/sample/messages",
	"views/modules/sample/revenue",
	"views/modules/sample/tasks",
	"views/modules/sample/map"
],function(dashline, visitors, orders, messages, revenue, tasks, map){

	var ui_scheme = {
		view:"scrollview",
        id:"dashboard",
        scroll:"y", //vertical scrolling
        body:{
			type: "clean",
			rows:[
				dashline,
				{
					type: "space",
					rows:[
						{
							height: 220,
							type: "wide",
							cols: [
								visitors,
								orders
							]
						},
						{
							type: "wide",
							cols: [
								messages,
								revenue

							]
						},
						{
							type: "wide",
							cols: [
								tasks,
								map
							]
						}
					]

				}
			]
        }
	};

	return { $ui:ui_scheme };

});
