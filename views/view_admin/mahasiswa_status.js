define([
	"views/modules/mhs_search",
	"views/modules/dataProcessing",
], function(search,handleProcessing){

var ui_scheme = {
    rows:[
    {
    	type: "material",
    	cols:[
    		search,
    		{
    			gravity:2.2,
    			rows:[
    				{
		    			view: "template",
		    			id:"tplprofile",
		    			scroll:true,
		    			template:"html-profile"
    				}
    			]
    		},
    	]
    }
    ],
};

return{
    $ui: ui_scheme,
    $oninit:function(){
        $$("tplprofile").bind( $$("listmsmhs"), "$data", function(data, source){
            if (data){
                this.load("laravel->/api/mahasiswa/"+data.id+"/status");
            }
        });

		var dp = new webix.DataProcessor({
		    updateFromResponse:true,
		    autoupdate:true,
		    master: $$("listmsmhs"),
		    url: "laravel->/api/mahasiswa/status",
		    on: handleProcessing
		});
	},
    $ondestroy:function(){
        $$("tplprofile").destructor();
    }
};
});
