define(function(){
	return {
		$ui:{
		view:"datatable",
		id:"list_semester",
		columns:[
			{ id: "id_smt", width:80,header:[{
				content:"serverFilter", placeholder:"smt",
			}]},
			{ id: "nm_smt", width:260,header:[{
				content:"serverFilter", placeholder:"Nama Semester",
			}]}
		],
		select:"row",
		url:"laravel->/api/semester"
		}
	}
});
