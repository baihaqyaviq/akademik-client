define([
    "views/modules/handle_processing"

	],function(handle){
	return {
		$ui:{
	    view:"datatable",
	    id:"grd_mata_kuliah",
	    columns:[
			{ id:"kode_mk", width: 100, header:[{
				content:"textFilter", placeholder:"Kode MK", compare:handle.startCompare
			}]},
	        { id:"nm_mk", width: 250, header:[{
				content:"textFilter", placeholder:"Nama MK", compare:handle.startCompare
			}]},
 	        { id:"nm_jns_mk", width: 180, header:[{
				content:"textFilter", placeholder:"Nama Jns MK", compare:handle.startCompare
			}]},
	        { id:"nm_kel_mk", width: 300,header:[{
				content:"textFilter", placeholder:"Nama Kel MK", compare:handle.startCompare
			}]},
	    ],
	    select:"multiselect",
	    dataFeed:true,
	    on:{
	        onAfterLoad:function(){
	        	var id = this.getFirstId();
	        	var record = this.getItem(id);
	            $$("lbl_win_detail").define("label", "MATAKULIAH " + record.nm_prodi.toUpperCase() );
	            $$('lbl_win_detail').refresh();
	        }
	    }
		}
	}
});
