define(function(){
	return {
        emptydata:function(){
            if (!this.count()){
                webix.extend(this, webix.OverlayBox);
                this.showOverlay("<div style='margin:75px; font-size:20px;'>There is no data</div>");
            }
        },
		onBeforeLoad:function(){
            this.showOverlay("Loading...");
        },
        onAfterLoad:function(view){
            view.hideOverlay();
            if (!view.count()){
                webix.extend(view, webix.OverlayBox);
                view.showOverlay("<div style='margin:75px; font-size:20px;'>There is no data</div>");
            }
        },
        numbering:{
            "data->onStoreUpdated":function(){
                this.data.each(function(obj, i){
                    obj.NO = i+1;
                })
            }
        },
        onAfterSync: function(id, text, data){
            var res = data.json();
            // var response = data.xml(), res = response.data;
            webix.message({ type:"default",
                text: "[MSG] " + res.action.type,
                expire:5000
            });
        },
        onAfterAdd : function(id, index, css, view){
            this.addRowCss(id,css);
            $$(view).addRowCss(id,css);
            this.sort("#id#", "desc", "int");
        },
        onAfterSaveError: function(id, status, res, details){
            // var operation = this.getItemState(id).operation; //operation that was performed
            // var response = details.text.xml();
            webix.modalbox({
                type:"alert-error",
                width: 500,
                height: 200,
                title:"[" + res.type+ "]" + " Error Messages",
                text: res.details,
                buttons:["Ok"],
            });
        },
        onBeforeDelete:function(e, id, node, view){
            webix.confirm({
                text:"The data will be deleted.<br/> Are you sure?", ok:"Yes", cancel:"Cancel",
                callback:function(res){
                    if(res){
                        $$(view).remove(id);
                    }
                }
            });
        },
        onSave:function(url){
            return {
                updateFromResponse:true,
                autoupdate:true,
                url: url,
                on:{
                    onAfterSync: this.onAfterSync,
                    onAfterSaveError: this.onAfterSaveError
                }
            };
        },
        startCompare:function(value, filter){
            value = value.toString().toLowerCase();
            filter = filter.toString().toLowerCase();
            return value.indexOf(filter) !== -1;
        }
    }
});
