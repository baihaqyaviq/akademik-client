define(function(){
	function startCompare(value, filter){
	    value = value.toString().toLowerCase();
	    filter = filter.toString().toLowerCase();
	    return value.indexOf(filter) !== -1;
	}

	return {
		$ui:{
	    view:"datatable",
	    id:"grid_mata_kuliah_semester",
	    columns:[
			{ id:"jml_kls", width: 100, header:"JML.Kelas", editor:"select", options:[1,2,3,4,5,6,7,8]},
			{ id:"smt", width: 80, header:[{
				content:"selectFilter", placeholder:"SMT",
			}]},
	    	{ id:"nm_kurikulum_sp", width: 150, header:[{
				content:"textFilter", placeholder:"Nama Kurikulum", compare:startCompare
			}]},
			{ id:"kode_mk", width: 100, header:[{
				content:"textFilter", placeholder:"Kode MK", compare:startCompare
			}]},
	        { id:"nm_mk", width: 250, header:[{
				content:"textFilter", placeholder:"Nama MK", compare:startCompare
			}]},
	        { id:"sks_mk", width: 80, header:[{
				content:"textFilter", placeholder:"SKS MK", compare:startCompare
			}]},

	    ],
	    select:"multiselect", editable:true,
	    datafetch:30,
	    dataFeed:function(id){
	        this.load("laravel->/api/semester/"+id+"/matakuliah");
	    },
		}
	}
});
