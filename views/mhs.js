define([
	"views/menus/profile",
	"models/model_mhs/sidebar_mhs",
	"views/webix/icon",
	"views/webix/menutree",
],function(profile, menu){

	var user_data = webix.storage.local.get("udata");

	//Top toolbar
	var mainToolbar = {
		view: "toolbar",
		css:"header",
		elements:[
			{view: "label", label: "Sistem Informasi Akademik", width: 200},

			{ height:46, id: "person_template", css: "header_person",
			borderless:true, width: 180, data: user_data,
				template: function(obj){
					var html = 	"<div style='height:100%;width:100%;' onclick='webix.$$(\"profilePopup\").show(this)'>";
					html += "<img class='photo' src='assets/imgs/photos/"+obj.userId+".png' /><span class='name'>"+obj.userName+"</span>";
					html += "<span class='webix_icon fa-angle-down'></span></div>";
					return html;
				}
			}
		]
	};

	var body = {
		rows:[
			{ height: 49, id: "title", css: "title", template: "<div class='header'>#title#</div><div class='details'>( #details# )</div>", data: {text: "",title: ""}},
			{ $subview:true }
		]
	};

	var layout = {
		rows:[
			mainToolbar,
			{
				cols:[
					menu,
					body
				]
			}
		]
	};

	return {
		$ui:layout,
		$menu:"sidebar:menu",
		$oninit:function(view, scope){
			// scope.ui(search.$ui);
			// scope.ui(mail.$ui);
			// scope.ui(message.$ui);
			scope.ui(profile.$ui);
		}
	};

});
