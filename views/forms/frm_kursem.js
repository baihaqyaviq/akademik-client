define(function(){
	return {
		$ui:{
		view: "form",
		id: "frm_kursem",
		scroll: true,
		elements: [
		{
			margin: 10,
			rows:[
				{ view:"text", name:"nm_kurikulum_sp", label: "nm_kurikulum_sp", labelWidth:100},
				{ view:"text", name:"jml_sem_normal", label: "jml_sem_normal", labelWidth:100},
				{ view:"text", name:"jml_sks_lulus", label: "jml_sks_lulus", labelWidth:100},
				{ view:"text", name:"jml_sks_wajib", label: "jml_sks_wajib", labelWidth:100},
				{ view:"text", name:"jml_sks_pilihan", label: "jml_sks_pilihan", labelWidth:100},
				{ view: "combo", name: "id_sms", label: "Prodi", gravity:1.5, labelWidth: "100", options:[
					{id:"9ae04002-37f2-46a5-8404-99e26721f056", value:"Teknik Sipil"},
					{id:"83a0b8ad-8da3-4b38-b33e-7e849e801021", value:"Teknik Informatika"},
					{id:"d0f55536-f20c-463e-8b5a-06eba731581a", value:"Arsitektur"},
					{id:"6e343592-475a-416d-8407-f77e6a3f8689", value:"Manajemen Informatika"},
					{id:"bab5b528-2ce7-4aab-b537-8fc72724d1f9", value:"Mesin Produksi"},
					{id:"833f76f5-282f-4d75-b4fa-549c5869ee7b", value:"Teknik Elektronika"},
				]},
				{ view: "combo", name: "id_jenj_didik", label: "Jenjang Pendidikan", labelWidth: "150",options:[
					{id: "22", value:"D3"},
					{id: "30", value:"S1"},
				]},
				{ view: "combo", name: "id_smt_berlaku", label: "id_smt_berlaku", labelWidth: "100", options: [
					{id:"20101", value:"2010/2011 Ganjil"},
					{id:"20102", value:"2010/2011 Genap"},
					{id:"20111", value:"2011/2012 Ganjil"},
					{id:"20112", value:"2011/2012 Genap"},
					{id:"20121", value:"2012/2013 Ganjil"},
					{id:"20122", value:"2012/2013 Genap"},
					{id:"20131", value:"2013/2014 Ganjil"},
					{id:"20132", value:"2013/2014 Genap"},
					{id:"20141", value:"2014/2015 Ganjil"},
					{id:"20142", value:"2014/2015 Genap"},
					{id:"20151", value:"2015/2016 Ganjil"},
					{id:"20152", value:"2015/2016 Genap"},
				]},
			]
		}
		]
		}
	}
});
