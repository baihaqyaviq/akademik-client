define(function(){
	return {
		$ui:{
		view: "form",
		id: "frm_matakuliah",
		scroll: true,
		elements: [
		{
			margin: 10,
			rows:[
			{
				cols:[
					{ view: "combo", name: "id_sms", label: "Prodi", gravity:1.5, labelWidth: "100", options:[
						{id:"9ae04002-37f2-46a5-8404-99e26721f056", value:"Teknik Sipil"},
						{id:"83a0b8ad-8da3-4b38-b33e-7e849e801021", value:"Teknik Informatika"},
						{id:"d0f55536-f20c-463e-8b5a-06eba731581a", value:"Arsitektur"},
						{id:"6e343592-475a-416d-8407-f77e6a3f8689", value:"Manajemen Informatika"},
						{id:"bab5b528-2ce7-4aab-b537-8fc72724d1f9", value:"Mesin Produksi"},
						{id:"833f76f5-282f-4d75-b4fa-549c5869ee7b", value:"Teknik Elektronika"},
					]},
					{ view: "combo", name: "id_jenj_didik", label: "Jenjang Pendidikan", labelWidth: "150",options:[
						{id: "22", value:"D3"},
						{id: "30", value:"S1"},
					]},
				]
			},
			{
				cols:[
					{ view:"text", name:"kode_mk",gravity:0.5, label: "kode_mk", labelWidth:100},
					{ view:"text", name:"nm_mk", label: "nm_mk", labelWidth:100},
				]
			},
			{
				cols:[
					{ view:"combo", name:"jns_mk", label: "jns_mk", labelWidth:100, options:[
						{ id: "A", value: "WAJIB"},
						{ id: "B", value: "PILIHAN"},
						{ id: "C", value: "WAJIB PEMINATAN"},
						{ id: "D", value: "PILIHAN PEMINATAN"},
						{ id: "S", value: "TA/SKRIPSI/TESIS/DISERTASI"}
					]},
					{ view:"combo", name:"kel_mk", label: "kel_mk", labelWidth:100, options:[
						{ id:"A", value:"MPK-PENGEMBANGAN KEPRIBADIAN"},
						{ id:"B", value:"MKK-KEILMUAN DAN KETRAMPILAN"},
						{ id:"C", value:"MKB-KEAHLIAN BERKARYA"},
						{ id:"D", value:"MPB-PERILAKU BERKARYA"},
						{ id:"E", value:"MBB-BERKEHIDUPAN BERMASYARAKAT"},
						{ id:"F", value:"MKU/MKDU"},
						{ id:"G", value:"MKDK"},
						{ id:"H", value:"MKK"}
					]},
				]
			},
			/*{
				cols:[
					{ view:"text", name:"sks_mk", label: "sks_mk", labelWidth:100},
					{ view:"text", name:"sks_tm", label: "sks_tm", labelWidth:100},
					{ view:"text", name:"sks_prak", label: "sks_prak", labelWidth:100},
					{ view:"text", name:"sks_prak_lap", label: "sks_prak_lap", labelWidth:100},
					{ view:"text", name:"sks_sim", label: "sks_sim", labelWidth:100},
				]
			},*/
			{
				cols:[
					{ view:"checkbox", name:"a_sap", label: "a_sap", labelWidth:100},
					{ view:"checkbox", name:"a_silabus", label: "a_silabus", labelWidth:100},
					{ view:"checkbox", name:"a_bahan_ajar", label: "a_bahan_ajar", labelWidth:100},
					{ view:"checkbox", name:"acara_prak", label: "acara_prak", labelWidth:100},
					{ view:"checkbox", name:"a_diktat", label: "a_diktat", labelWidth:100},
				]
			},
			{
				cols:[
					{ view:"datepicker", name:"tgl_mulai_efektif", label: "tgl mulai efektif", labelWidth:150},
					{ view:"datepicker", name:"tgl_akhir_efektif", label: "tgl akhir efektif", labelWidth:150},
				]
			}
			]
		}
		]
		}
	}
});
