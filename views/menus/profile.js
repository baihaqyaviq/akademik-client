define(function(){

return {
	$ui:{
		view: "submenu",
		id: "profilePopup",
		width: 200,
		padding:0,
		data: [
			{id:"logout", icon: "sign-out", url:"#!/logout", value: "Logout"}
		],
		type:{
			template: function(obj){
				if(obj.type)
					return "<div class='separator'></div>";
				return "<span class='webix_icon alerts fa-"+obj.icon+"'></span><span>"+obj.value+"</span>";
			}
		},
		on:{
			onItemClick:function(id){
				var item = this.getItem(id);
				document.location.href = item.url;
			}
		},

	}
};

});
