define([
	"views/forms/frm_mhs_biodata",
], function(forms){

var button ={
	view: "form",
	paddingX:5,
	paddingY:5,
	height:40,
	cols:[
		{},
		{ view: "button", css: "button_raised", label: "Update", width: 90, click:function(obj){
			// webix.$$("winmsmhs").show();
			// $$("btsubmit").setValue("update");
		}},
	]
}

var title = {
	view: "toolbar",
	css: "highlighted_header header3",
	paddingX:5,
	paddingY:5,
	height:40,
	cols:[
		{
			template: "<span class='webix_icon fa-star-o'></span>Nama", "css": "sub_title2", borderless: true
		},
	]
};

var grd_biodata={
	view:"datatable",
	id:"grd_biodata",
	header:false,
	columns:[
		{ id:"column", width:200},
		{ id:"fill", width:80},
	],
	data: [
		{ id:1, column:"Nama", fill:1994 },
		{ id:2, column:"Nim", fill:1972 }
	]
};

var ui_scheme = {
	type: "material",
    rows:[
    {
		id:"profile",
		cols:[
			{
				view:"template",
				template: function(obj){
					return 	"<div style='margin: auto;'><img style='margin: auto;' src='assets/imgs/mhs_photo/undefined.jpg' /></div>";
				}
			},
			{
				gravity:3.0,
				margin_top:10,
				rows: [
					forms,
					button
				]
			}
		]
	},
    ],
};

return {
    $ui: ui_scheme,
    $oninit: function(){
		// var result = webix.ajax().sync().get("laravel->/api/mahasiswa/"); // id_reg_pd
		// $$("tplprofile").parse(result.responseText);
		// $$("formMsmhs").parse(result.responseText);

		/*$$("formMsmhs").attachEvent("onAfterValidation", function(result, value){
		    console.log(value);
		});*/
	}
};
});
