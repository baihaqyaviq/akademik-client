define([
	"models/model_mhs/dashline",
	"models/model_mhs/mahasiswa",
	"models/model_mhs/sebaran_nilai",
	"models/model_mhs/penghasilan_ortu",
],function(dashline, mahasiswa, sebaran_nilai, penghasilan_ortu){

	var ui_scheme = {
		type: "line",
		view:"scrollview",
        id:"dashboard",
        scroll:"y", //vertical scrolling
        body:{
			rows:[
				dashline,
				{
					type: "space",
					rows:[
						mahasiswa,
						{
							height: 220,
							type: "wide",
							cols: [
								sebaran_nilai,
								penghasilan_ortu
							]
						}
					]
				}
			]
		}
	};

	return {
	    $ui: ui_scheme
	};

});
