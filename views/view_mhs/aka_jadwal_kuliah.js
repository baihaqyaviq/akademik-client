define([
	"views/modules/dataProcessing"
],function(handleProcessing){

	function equals(a,b){
		a = a.toString().toLowerCase();
		return a.indexOf(b) !== -1;
	}

	function oneForAll(value, filter, obj){
		if (equals(obj.start_date, filter)) return true;
		if (equals(obj.event_name, filter)) return true;
		return false;
	};

	var grd_jadwal = {
		view:"datatable",
		id:"grd_jadwal",
		columns:[
	        { id:"No", header: "No", width:80 },
	        { id:"Hari", header: "Hari", width:80 },
	        { id:"Waktu", header: "Waktu", width:80 },
	        { id:"Prodi", header: "Prodi", width:80 },
	        { id:"Smt", header: "Smt", width:80 },
	        { id:"Mata", header: "Mata", width:80 },
	        { id:"Kuliah", header: "Kuliah", width:80 },
	        { id:"SKS", header: "SKS", width:80 },
	        { id:"Dosen", header: "Dosen", width:80 },
	        { id:"Pengampu", header: "Pengampu", width:80 },
	        { id:"Ruang", header: "Ruang", width:80 },
	        { id:"Keterangan", header: "Keterangan", width:80 }
		]
	};

	var buttons = {
		view:"toolbar", elements:[
			{ view:"button", value:"Add Row", click:function(){
				$$('datatable1').add({
					start_date:"2015-08-30",
					end_date  :"2015-12-30",
					event_name:"Wisuda event"
				});
			}},
			{ view:"button", value:"Delete Row", click:function(){
				var id = $$('datatable1').getSelectedId();
				if (id)
					$$('datatable1').remove(id);
			}},
			{}
		]
	};

	var ui_scheme = {
		type: "material",
	    rows:[
		    grd_jadwal
	    ],
	};

	return {
	    $ui: ui_scheme,
	    $oninit:function(){
	    	var dp = new webix.DataProcessor({
	            updateFromResponse:true,
	            autoupdate:true,
	            master: $$("grd_jadwal"),
	            url: "laravel->/test.json",
	            on: handleProcessing
	        });
	    }
	};

});
