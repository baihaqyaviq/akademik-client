define([
	"views/modules/dataProcessing"
],function(handleProcessing){

	function equals(a,b){
		a = a.toString().toLowerCase();
		return a.indexOf(b) !== -1;
	}

	function oneForAll(value, filter, obj){
		if (equals(obj.start_date, filter)) return true;
		if (equals(obj.event_name, filter)) return true;
		return false;
	};

	var grd_kursem = {
		view:"datatable",
		id:"grd_kursem",
		columns:[
	        { id:"smt", header: "smt", width:80 },
	        { id:"a_wajib", header: "a_wajib", width:80 },
	        { id:"kode_mk", header: "kode_mk", width:80 },
	        { id:"nm_mk", header: "nm_mk", width:80 },
	        { id:"sks_mk", header: "sks_mk", width:80 },
	        { id:"sks_tm", header: "sks_tm", width:80 },
	        { id:"sks_prak", header: "sks_prak", width:80 },
	        { id:"sks_prak_lap", header: "sks_prak_lap", width:80 },
	        { id:"sks_sim", header: "sks_sim", width:80 },
		]
	};

	var buttons = {
		view:"toolbar", elements:[
			{ view:"button", value:"Add Row", click:function(){
				$$('datatable1').add({
					start_date:"2015-08-30",
					end_date  :"2015-12-30",
					event_name:"Wisuda event"
				});
			}},
			{ view:"button", value:"Delete Row", click:function(){
				var id = $$('datatable1').getSelectedId();
				if (id)
					$$('datatable1').remove(id);
			}},
			{}
		]
	};

	var ui_scheme = {
		type: "material",
	    rows:[
		    grd_kursem
	    ],
	};

	return {
	    $ui: ui_scheme,
	    $oninit:function(){
	    	var dp = new webix.DataProcessor({
	            updateFromResponse:true,
	            autoupdate:true,
	            master: $$("grd_kursem"),
	            url: "laravel->/test.json",
	            on: handleProcessing
	        });
	    }
	};

});
