define([
	"views/modules/handle_processing"
],function(handle){

var user_data = webix.storage.local.get("udata");

var title2 = {
	view: "toolbar",
	css: "highlighted_header header4",
	paddingX:5,
	paddingY:5,
	height:40,
	cols:[
		{
			template: "<span class='webix_icon fa-star-o'></span>Matakuliah yang diambil", "css": "sub_title2", borderless: true
		},
		{ view: "button", type: "iconButton", css:"button_transparent", icon: "print", label: "Cetak", width: 100, click:function(){
			var value = {id:"000371fd-3e76-4a06-91e5-b9600e665e7d", smt:"20142"}; //id_reg_pd. id_smt
			webix.send("http://localhost:8000/print/krs",value, 'POST', '_blank');
		}}
	]
};

var grd_her = {
	view:"datatable",
	select:"multiselect",
	id:"grd_her",
	url: "laravel->/api/mahasiswa/000371fd-3e76-4a06-91e5-b9600e665e7d/nilaiher", // id_reg_pd
	columns:[
		{ id:"kode_mk",	header:[{content:"textFilter", placeholder:"Kode"}], width:100},
		{ id:"nm_mk",	header:[{content:"textFilter", placeholder:"Nama Matakuliah"}], width:200 },
		{ id:"sks_mk",	header:"SKS", width:70 },
		{ id:"nilai_huruf",	header:"Huruf", width:70 },
		{ id:"nilai_indeks",	header:"Indeks", width:70 },
		{ id:"add", header:"Ambil", width:70, template:"<span  style=' cursor:pointer;' class='webix_icon fa-plus-square text_success'></span>"},
	],
	onClick: {
		'fa-plus-square': function (e, id, node) {
			$$('grd_ambil').add(this.getItem(id));
			$$("grd_her").remove(id);
		}
	},
	on:{
		onAfterAdd : function(id, index){
			this.addRowCss(id,"bg_warning");
		},
		onItemDblClick: function (id) {
			$$('grd_ambil').add(this.getItem(id));
			$$("grd_her").remove(id);
		}
	}
};

var grd_tawar = {
	view:"datatable",
	select:"multiselect",
	id:"grd_tawar",
	url: "laravel->/api/83a0b8ad-8da3-4b38-b33e-7e849e801021/mktawar/20151", //id_sms, id_smt
	columns:[
		{ id:"smt",	header:"SMT", width:70 },
		{ id:"kode_mk",	header:[{content:"textFilter", placeholder:"Kode"}], width:100},
		{ id:"nm_mk",	header:[{content:"textFilter", placeholder:"Nama Matakuliah"}], width:200 },
		{ id:"sks_mk",	header:"SKS", width:70 },
		{ id:"add", header:"Ambil", width:70, template:"<span  style=' cursor:pointer;' class='webix_icon fa-plus-square text_success'></span>"},
	],
	onClick: {
		'fa-plus-square': function (e, id, node) {
			$$('grd_ambil').add(this.getItem(id));
			$$("grd_tawar").remove(id);
		}
	},
	on:{
		onAfterAdd : function(id, index){
			this.addRowCss(id,"bg_info");
		},
		onItemDblClick: function (id) {
			$$('grd_ambil').add(this.getItem(id));
			$$("grd_tawar").remove(id);
		}
	}
};
var grd_ambil = {
	view: "datatable",
	id: "grd_ambil",
	select:true,
	footer:true,
	url:"laravel->/api/mahasiswa/000371fd-3e76-4a06-91e5-b9600e665e7d/krs/20151", //id_reg_pd, id_smt
	columns:[
		{ id:"trash", header:"&nbsp;", width:35, template:"<span  style='cursor:pointer;' class='webix_icon fa-trash-o text_danger'></span>"},
		{ id:"kode_mk",	header:[{content:"textFilter", placeholder:"Kode"}], width:100},
		{ id:"nm_mk",	header:[{content:"textFilter", placeholder:"Nama Matakuliah"}], width:200, footer:"Total SKS" },
		{ id:"sks_mk",  header:"SKS", width:70, footer:{content:"summColumn"}},
		{ id:"nm_kls",	header:"KLS", width:70 },
		{ id:"is_her",	header:"HER", width:70 },

	],
	onClick:{
		webix_icon:function(e,id,node){

			webix.confirm({
				text:"The record will be deleted.<br/> Are you sure?", ok:"Yes", cancel:"Cancel",
				callback:function(res){
					if(res){

						var ambil = $$("grd_ambil").getItem(id);

						if(ambil.is_her === null){
							$$('grd_tawar').add(ambil);
						}else{
							$$('grd_her').add(ambil);
						}

						$$("grd_ambil").remove(id);
					}
				}
			});
		}
	},
	on:{
		onAfterAdd : function(id, index){
			var ambil = $$("grd_ambil").getItem(id);
			var css = (ambil.is_her === null) ? 'bg_info': 'bg_warning' ;

			this.addRowCss(id, css);
		},
		onItemDblClick: function (id) {
			webix.confirm({
				text:"The record will be deleted.<br/> Are you sure?", ok:"Yes", cancel:"Cancel",
				callback:function(res){
					if(res){
						var ambil = $$("grd_ambil").getItem(id);

						if(ambil.is_her === null){
							$$('grd_tawar').add(ambil);
						}else{
							$$('grd_her').add(ambil);
						}

						$$("grd_ambil").remove(id);
					}
				}
			});
		}
	}
};

var ui_scheme ={
    rows:[
    {
    	margin:10,
		type:"material",
    	cols:[
    	{
    		view:"accordion",
		    rows:[
		    {
		      	view:"accordionitem",
				header: "<span class='text_warning'><i class='webix_icon fa-star'></i>Matakuliah yang akan di ulang </span>",
		      	headerHeight:50,
		      	body: grd_her,
		      	collapsed: true
		    },
		    {
		      	view:"accordionitem",
				header: "<span class='text_info'> <i class='webix_icon fa-star text_info'></i>Matakuliah yang ditawarkan </span>",
		      	body: grd_tawar,
		    }]

    	},
    	{
			rows:[
				title2,
	    		grd_ambil
			]
    	}
    	]
    }
    ]
}

return {
    $ui: ui_scheme,
    $oninit: function(){
		var dp = new webix.DataProcessor({
		    updateFromResponse:true,
		    autoupdate:true,
		    master: $$("grd_ambil"),
		    url: "laravel->/api/mahasiswa/000371fd-3e76-4a06-91e5-b9600e665e7d/krs", // id_reg_pd
		    /*on:{
                onAfterSync: handle.onAfterSync,
                onAfterSaveError: handle.onAfterSaveError
            }*/
		});
	}
};
});
