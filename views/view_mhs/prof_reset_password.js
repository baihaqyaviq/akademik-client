define([
	"views/modules/dataProcessing"
],function(handleProcessing){

	var form={
		view: "form",
		id: "form_reset",
		elementsConfig:{
			labelWidth: 120
		},
		elements: [
			{view: "text",type:"password", name: "old_password", label: "Password Lama"},
			{view: "text",type:"password", name: "new_password", label: "Password Baru"},
			{view: "text",type:"password", name: "re_password",	label: "Password Baru"},
			{
				margin: 10,
				paddingX: 2,
				borderless: true,
				cols:[
					{},
					{view: "button", label: "Reset", css: "button_raised", align: "right", width: 90, height:35, click:function(){
						$$('form_reset').clear();
					}},
					{view: "button", label: "Save", css: "button_success button_raised", type: "form", align: "right", width: 90, height:35, click:function(){
						var form = $$('form_reset');
						var values = form.getValues();

						if(form.isDirty()){
							if(!form.validate())
								return false;

							webix.ajax().post("./resetpassword.json", values, function(t,res) {
								var res = res.json();

								webix.message({ type:"default",
									text: res.messages.data+"<br>"+res.messages.status +" in "+res.messages.operation,
									expire:5000
								});
							});
				        };
					}}
				]
			}
		]
	}

	var ui_window = {
	    view:"window",
	    id:"win_reset",
	    modal:true,
	    position:"center",
	    width:700,
	    height:500,
	    head:{
	        view:"toolbar", margin:-4, cols:[
	            { view:"label", id:"lbl_win_reset", label: "RESET PASSWORD" },
	            { view:"icon", icon:"question-circle", click:"webix.message('About pressed')"},
	            { view:"icon", icon:"times-circle", click:function(){
	                this.getTopParentView().hide();
	            }}
	        ]
	    },
	    body: {
	        rows:[form]
	    }
	};

	return {
		$ui:null,
	    $windows:[ui_window],
	    $oninit:function(){
	    	$$("win_reset").show();
	    }
	};

});
