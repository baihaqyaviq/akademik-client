define(["models/user"],function(user){
    var config = {
        provider :"http://localhost:8000",
    };

    return{
        $oninit:function(app, newconfig){

            if (newconfig)
                webix.extend(config, newconfig, true);

            config.provider = (app.config.provider) ? app.config.provider : config.provider;


            webix.proxy.laravel = {
                $proxy:true,
                dynamicLoading:true,

                init:function(){
                    // webix.extend(this, webix.proxy.rest);

                    var credentials = user.getCurrentUser();

                    webix.attachEvent("onBeforeAjax", function(mode, url, params, x, headers){

                        var getLocation = function(href) {
                            var l = document.createElement("a");
                            l.href = href;
                            return l;
                        };

                        var location = getLocation(url);
                        var paths = location.pathname.split("/");

                        if(paths.indexOf("login") < 0 ){
                            headers["Authorization"] ="Bearer "+ credentials;
                        }
                    });
                },

                load:function(view, callback, details){
                    //http://localhost:8000/api/mahasiswa?continue=true&count=50&start=1&filter[nipd]=0755&filter[nm_pd]=fahri

                    var url = config.provider + this.source;

                    /*if(this.dynamicLoading){
                        url += "&inlinecount=allpages";
                        if(!details) url+="&count="+view.config.datafetch;
                    }*/

                    // if server-side sorting or filtering is triggered
                    if(details){
                        url += (url.indexOf("?") == -1) ? "?" : "&";
                        url += "continue=true";

                        var start = details.from, count = details.count;

                        if(start && count!==-1){
                            if(start<0) start = 0;
                            url += "&count="+count+"&start="+start;
                        }

                        if(details.sort)
                            url += "&orderby="+details.sort.id+" "+details.sort.dir;

                        if(details.filter){
                            var filters = [];
                            for (var key in details.filter){
                                if(details.filter[key])
                                    filters.push("filter["+key+"]="+(details.filter[key]?details.filter[key]:""));
                            }
                            if(filters.length)
                                url +="&"+filters.join("&");
                        }
                    }
                    webix.ajax(url, callback, view);
                },

                save:function(view, update, dp, callback){
                    var url = config.provider + this.source;
                    // console.log(url.charAt(url.length-1));

                    var mode = update.operation;

                    if(mode !== 'insert')
                        url += (url.charAt(url.length-1) == "/") ? "" : "/"; // fix me

                    var data = update.data;
                    if (mode == "insert") delete data.id;

                    //call rest URI
                    if (mode == "update"){
                        webix.ajax().put(url + data.id, data, callback);
                    } else if (mode == "delete") {
                        webix.ajax().del(url + data.id, data, callback);
                    } else {
                        webix.ajax().post(url, data, callback);
                    }
                }
            };

            webix.attachEvent("onLoadError", function(text, xml, xhttp, owner){
                text = text || "[EMPTY DATA]";
                var error_text = "Data loading error, check console for details";
                if (text.indexOf("<?php") === 0)
                    error_text = "PHP support missed";
                else if (text.indexOf("WEBIX_ERROR:") === 0)
                    error_text = text.replace("WEBIX_ERROR:","");

                if (webix.message)
                    webix.message({
                        type:"error",
                        text:error_text,
                        expire:-1
                    });
                if (window.console){
                    var logger = window.console;
                    logger.log("Data loading error");
                    logger.log("Object:", owner);
                    logger.log("Response:", text);
                    logger.log("XHTTP:", xhttp);
                }
            });

            //fix me
            /*webix.attachEvent("onAfterSync", function(id, text, data){
                // var res = data.json();
                var response = data.xml(), res = response.data;
                webix.message({ type:"default",
                    text: "[MSG] " + res.action.type,
                    expire:5000
                });
            });

            webix.attachEvent("onAfterSaveError",function(id, status, res, details){
                // var operation = this.getItemState(id).operation; //operation that was performed
                // var response = details.text.xml();
                webix.modalbox({
                    type:"alert-error",
                    width: 500,
                    height: 200,
                    title:"[" + res.type+ "]" + " Error Messages",
                    text: res.details,
                    buttons:["Ok"],
                });
            });*/

        }
    }
});
