/*
	App configuration
*/

define([
	"libs/webix-jet-core/core",
	"libs/webix-jet-core/plugins/menu",
	"libs/webix-jet-core/plugins/locale",
	"libs/webix-jet-core/plugins/user",
	"plugins/laravel_proxy"
], function(
	core, menu, locale, user, proxy
){

	//configuration
	var app = core.create({
		id:         "Akademik",
		name:       "Akademik",
		version:    "0.1.0",
		debug:      true,
		start: 		"/mhs/view_mhs.dashboard",
		provider:   "http://localhost:8000",
	});

	app.use(menu);
	app.use(locale);
	app.use(user);
	app.use(proxy);
	return app;
});
