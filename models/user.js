/**
 * Created by Mads on 02-10-2015.
 */
define(function() {

    "use strict";

    var credentials = {} ;

    credentials = Cookies.getJSON("appCookie") || {};

    //console.log("Credentials on load: ", credentials);

    return {
        getCurrentUser: function() {
            return this.getAccessToken();
        },

        decodeToken:function(){

            var access_token = credentials.jwt.split(".");
            return  JSON.parse (b64utos(access_token[1]) );
        },

        getAccessToken: function() {
            // console.log(credentials);

            if( credentials.jwt ){
                var decodeJWT = this.decodeToken();

                var crd = {};

                crd.iat = [new Date(decodeJWT.iat * 1000), decodeJWT.iat];
                crd.nbf = [new Date(decodeJWT.nbf * 1000), decodeJWT.nbf];
                crd.exp = [new Date(decodeJWT.exp * 1000), decodeJWT.exp];
                crd.crt = [new Date(Date.now()), Date.now()];

                webix.storage.local.put("udata", decodeJWT.data);

                // console.log(crd);

                if (decodeJWT.jti === undefined) {
                    console.log('jti not found');
                    return null;
                }

                // console.log( Date.now() +" - "+ decodeJWT.exp * 1000 );

                if ( (Date.now()) > (decodeJWT.exp * 1000) ) {
                    console.log('token is expired');
                    return null;
                }

                return credentials.jwt;
            }

            console.log('jwt not found');
            return null;
        },

        login: function(username, password, component) {

            webix.extend(component, webix.ProgressBar);
            component.disable();
            component.showProgress();

            webix.ajax().post("http://localhost:8000/login", {
                "username": username,
                "password": password
            }, function(text, data, xhr) {
                credentials = data.json();
                Cookies.set("appCookie", credentials);

            }).then(function() {

                setTimeout(function() {
                    component.enable();
                    component.hideProgress();
                }, 1000);

                webix.message({ type:"success",
                    text: "welcome",
                    expire:5000
                });

                require(["app"], function(app){
                    // app.router(app.config.start);
                    window.location.reload("#!" + app.config.start);
                });

            }).fail(function(){
                webix.message({ type:"error",
                    text: "error system",
                    expire: -1
                });
            });

            /*if (username == "test" && password == "test") {
                credentials = {
                    "access_token": "9c5742da1edc3531da2009fb35bb843c49e2e680",
                    "expires_in": 3600,
                    "token_type": "Bearer",
                    "scope": null,
                    "refresh_token": "1a8ceb5b59dac24f532b852e544ec3b834cea53c"
                };
                credentials.timestamp = Date.now() / 1000 | 0;

                Cookies.set("appCookie", credentials);

                require(["app"], function(app){
                    app.router(app.config.start);
                });
            } else {
                setTimeout(function() {
                    component.enable();
                    component.hideProgress();
                }, 2000);
            }*/
        },

        logout: function() {
            credentials = {};
            Cookies.remove("appCookie");
            webix.storage.local.remove("udata");
        },

        refresh: function() {
            // Use the refresh-token to get a new bearer-token
        },

        session: {
            status: function() {
                return new Promise(function (resolve, reject) {
                    console.log("Getting user-status");
                    resolve(true);
                });
            }
        }
    };
});
