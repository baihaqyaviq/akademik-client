define(function(){
	var dataMenu = new webix.DataCollection({ data:[
        // {id: "main", value: "Main", open: true, data:[
        //     { id: "view_admin.dashboard", value: "Dashboard", icon: "home", $css: "dashboard", details:"reports and statistics"}
        // ]},
		// {id : "m0", url : "", value : "Mahasiswa", details : "Mahasiswa", open :  false, data : [
            // {id : "view_admin.mahasiswa", value : "Daftar Mahasiswa", icon : "check-square-o", details : "Daftar Mahasiswa"},
            // {id : "view_admin.mahasiswa_biodata", value : "Biodata Mahasiswa", icon : "check-square-o", details : "Biodata Mahasiswa"},
            // {id : "view_admin.mahasiswa_pendidikan", value : "Riwayat Pendidikan ", icon : "check-square-o", details : "Riwayat Pendidikan Mahasiswa"},
            // {id : "view_admin.mahasiswa_krs", value : "Krs Mahasiswa", icon : "check-square-o", details : "Krs Mahasiswa"},
            // {id : "view_admin.mahasiswa_nilai", value : "Nilai Mahasiswa", icon : "check-square-o", details : "Nilai Mahasiswa"},
            // {id : "view_admin.mahasiswa_aktifitas", value : "Aktifitas Perkuliahan", icon : "check-square-o", details : "Aktifitas Perkuliahan Mahasiswa"},
            // {id : "view_admin.mahasiswa_registrasi", value : "Status Registrasi", icon : "check-square-o", details : "Status Registrasi Mahasiswa"}
        // ]},
        // {id : "d0", url : "", value : "Dosen", details : "Dosen", open :  false, data : [
            // {id : "view_admin.dosen", value : "List Dosen", icon : "check-square-o", details : "List Dosen"},
            // {id : "view_admin.dosen_biodata", value : "Detail Dosen", icon : "check-square-o", details : "Detail Dosen"},
            // {id : "view_admin.dosen_penugasan", value : "Penugasan Dosen", icon : "check-square-o", details : "Penugasan Dosen"},
            // {id : "view_admin.dosen_aktifitas", value : "Aktifitas Mengajar", icon : "check-square-o", details : "Aktifitas Mengajar"},
            // {id : "view_admin.dosen_pendidikan", value : "Riwayat Pendidikan", icon : "check-square-o", details : "Riwayat Pendidikan"},
            // {id : "view_admin.dosen_struktural", value : "Riwayat Struktural", icon : "check-square-o", details : "Riwayat Struktural"},
            // {id : "view_admin.dosen_fungsional", value : "Riwayat Fungsional", icon : "check-square-o", details : "Riwayat Fungsional"},
            // {id : "view_admin.dosen_kepangkatan", value : "Riwayat Kepangkatan", icon : "check-square-o", details : "Riwayat Kepangkatan"},
            // {id : "view_admin.dosen_sertifikasi", value : "Riwayat Sertifikasi", icon : "check-square-o", details : "Riwayat Sertifikasi"}
        // ]},
        {id : "k0", url : "", value : "Perkuliahan", details : "Perkuliahan", open :  false, data : [
            {id : "view_admin.matakuliah", value : "List Matakuliah", icon : "check-square-o", details : "List Matakuliah"},
            {id : "view_admin.kuliah_kurikulum", value : "Kurikulum Semester", icon : "check-square-o", details : "Kurikulum Semester"},
            // {id : "view_admin.kuliah_subtansi", value : "List Subtansi Kuliah", icon : "check-square-o", details : "List Subtansi Kuliah"},
            {id : "view_admin.kuliah_listkelas", value : "List Kelas", icon : "check-square-o", details : "Add Kelas Perkuliahan"},
            {id : "view_admin.kuliah_kelas", value : "Kelas Perkuliahan", icon : "check-square-o", details : "Kelas Perkuliahan"},
            {id : "view_admin.kuliah_nilai", value : "Nilai Perkuliahan", icon : "check-square-o", details : "Nilai Perkuliahan"},
            // {id : "view_admin.kuliah_aktifitasmhs", value : "Aktifitas Kuliah MHS", icon : "check-square-o", details : "Aktifitas Kuliah Mahasiswa"},
            // {id : "view_admin.kuliah_statusmhs", value : "Mahasiswa Lulus/DO", icon : "check-square-o", details : "Mahasiswa Lulus/droput"}
        ]},
        // {id : "t0", url : "", value : "Tools", details : "Tools", open :  false, data : [
        //     {id : "view_admin.tools_sync", value : "Sinkronisasi", icon : "check-square-o", details : "Sinkronisasi Data"},
        // ]}
    ]});

	return {
		$ui:{
			width: 200,

			rows:[
				{
					view: "tree",
					id: "sidebar:menu",
					type: "menuTree2",
					css: "menu",
					activeTitle: true,
					select: true,
					tooltip: {
						template: function(obj){
							return obj.$count?"":obj.details;
						}
					},
					on:{
						onBeforeSelect:function(id){
							if(this.getItem(id).$count){
								return false;
							}

						},
						onAfterSelect:function(id){
							this.$scope.show("./"+id);
							var item = this.getItem(id);
							webix.$$("title").parse({title: item.value, details: item.details});
						}
					},
					data:dataMenu
				}
			]
		}
	};

});
