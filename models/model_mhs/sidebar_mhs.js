define(function(){
	var dataMenu = new webix.DataCollection(
        {
            data:[
                {value:"Profil",id:"profil",details:"Profil",published:"1",type:"back",group_id:"2","data":[
                    {id:"view_mhs.prof_biodata",value:"Biodata",details:"Biodata Mahasiswa","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.prof_reset_password",value:"Reset Password",details:"Reset Password","icon":"check-square-o",published:"1",type:"back",group_id:"2"}
                ]},
                {value:"Akademik",id:"akademik",details:"Akademik",published:"1",type:"back",group_id:"2","data":[
                    {id:"view_mhs.aka_transkrip_nilai",value:"Transkrip Nilai",details:"Trankrip Nilai","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.aka_khs",value:"Kartu Hasil Studi",details:"Kartu Hasil Studi","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.aka_krs",value:"Kartu Rencana Studi",details:"kartu Rencana Studi","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.aka_kurikulum",value:"Kurikulum",details:"Kurikulum ","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.aka_jadwal_kuliah",value:"Jadwal Kuliah",details:"Jadwal Kuliah","icon":"check-square-o",published:"1",type:"back",group_id:"2"}
                ]},
                /*{value:"Status Pembayaran",id:"status",details:"Status Pembayaran",published:"1",type:"back",group_id:"2","data":[
                    {id:"view_mhs.status_tagihan",value:"Informasi Tagihan",details:"Informasi Tagihan","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.status_registrasi",value:"Status Registrasi",details:"Status Registrasi","icon":"check-square-o",published:"1",type:"back",group_id:"2"}
                ]},
                {value:"Pendaftaran Kegiatan",id:"daftar",details:"Pendaftaran Kegiatan",published:"1",type:"back",group_id:"2","data":[
                    {id:"view_mhs.keg_peserta_seminar",value:"Peserta Seminar",details:"Pendaftaran Peserta Seminar","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_kkl",value:"KKL",details:"Kuliah Kerja Lapangan","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_kp",value:"Kerja Praktek",details:"Kerja Praktek","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_kpm",value:"KPM",details:"Kuliah Pengabdian Masyarakat","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_skripsi",value:"Skripsi",details:"Pendaftaran Skripsi","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_pendadaran",value:"Pendadaran",details:"Pendaftaran Pendadaran","icon":"check-square-o",published:"1",type:"back",group_id:"2"},
                    {id:"view_mhs.keg_wisuda",value:"Wisuda",details:"Pendaftaran Wisuda","icon":"check-square-o",published:"1",type:"back",group_id:"2"}
                ]}*/
            ]
        });

	return {
		$ui:{
			width: 200,

			rows:[
				{
					view: "tree",
					id: "sidebar:menu",
					type: "menuTree2",
					css: "menu",
					activeTitle: true,
					select: true,
					tooltip: {
						template: function(obj){
							return obj.$count?"":obj.details;
						}
					},
					on:{
						onBeforeSelect:function(id){
							if(this.getItem(id).$count){
								return false;
							}

						},
						onAfterSelect:function(id){
							this.$scope.show("./"+id);
							var item = this.getItem(id);
							webix.$$("title").parse({title: item.value, details: item.details});
						}
					},
					data:dataMenu
				}
			]
		}
	};

});
