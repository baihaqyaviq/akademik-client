var debug_export = false;

var gulp = require('gulp');
var glob = require('glob');

var _if = require('gulp-if');
var rjs = require('gulp-requirejs');
var uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');
var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');
var rimraf = require('gulp-rimraf');
var replace = require("gulp-replace");
var jshint = require("gulp-jshint");
var argv = require('yargs').argv;
var taskListing = require('gulp-task-listing');

gulp.task('help', taskListing);

gulp.task("css", function(){
    return build_css();
});

function build_css(type){
    return gulp.src('./assets/css/*.css')
        .pipe(uglifycss({
            "max-line-len": 80
        }))
        .pipe(gulp.dest("./deploy/"+type+"/assets/css"));
}

gulp.task('js', function(){
    var type = (argv.admin) ? 'admin' : 'mhs';
    return build_js(type);
});

function build_js(type){
    // var view_type  = (type == 'admin') ? "view_admin" : "view_mhs";
    // var model_type = (type == 'admin') ? "model_admin" : "model_mhs";
    var afterLogin = (type == 'admin') ? "#!/adm/dashboard" : "#!/mhs/dashboard";

    if(type == 'admin'){
        gulp.src(['views/adm.js', 'views/login.js'])
            .pipe(gulp.dest("./views/view_"+type+"/"));
    }

    if(type == 'mhs'){
        gulp.src(['views/mhs.js', 'views/login.js'])
            .pipe(gulp.dest("./views/view_"+type+"/"));
    }

    // folder list
    var views = glob.sync("views/view_"+ type +"/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var forms = glob.sync("views/forms/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var menus = glob.sync("views/menus/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var modules = glob.sync("views/modules/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var webix = glob.sync("views/webix/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var locales = glob.sync("locales/**/*.js").map(function(value){
        return value.replace(".js", "");
    });

    var models = glob.sync("models/model_"+ type +"/*.js").map(function(value){
        return value.replace(".js", "");
    });

    // console.log(views);

    return rjs({
        baseUrl: './',
        out: 'app.js',
        insertRequire:["app"],
        paths:{
            "locale" : "empty:",
            "text": 'libs/text'
        },
        deps:["app"],
        include: ["libs/almond/almond.js"]
        .concat(views)
        .concat(forms)
        .concat(menus)
        .concat(modules)
        .concat(webix)
        .concat(models)
        .concat(locales)
    })
    .pipe( _if(debug_export, sourcemaps.init()) )
    .pipe(uglify())
    .pipe( _if(debug_export, sourcemaps.write("./")) )
    .pipe(replace("views\/view_"+type, 'views'))
    .pipe(replace("view_"+ type + ".", ""))
    .pipe(replace("models\/model_"+type, 'models'))
    .pipe(replace('afterLogin:"#!/adm/view_admin.dashboard"', 'afterLogin: "'+ afterLogin +'"'))
    .pipe(gulp.dest("./deploy/"+type+"/"));
}

gulp.task("clean", function(){
    var type = (argv.admin) ? 'admin' : 'mhs';
    return gulp.src("deploy/"+type+"/*", {read: false}).pipe(rimraf());
});


/**
 * build command gulp build --mhs --debug or gulp build --admin --debug
 * @param  {[type]} ){var build [description]
 * @return {[type]}     [description]
 */
gulp.task('build', ["clean"], function(){
    var build = (new Date())*1;
    var type = (argv.admin) ? 'admin' : 'mhs';
    var webix = (argv.debug) ? 'webix_debug' : 'webix';

    return require('event-stream').merge(
    build_js(type),
    build_css(type),
        //assets
    gulp.src("./assets/imgs/**/*.*")
        .pipe(gulp.dest("./deploy/"+type+"/assets/imgs/")),

        //fonts
    gulp.src('./assets/fonts/**/*')
        .pipe(gulp.dest("./deploy/"+type+"/assets/fonts/")),

        //cookie
    gulp.src('libs/js-cookie/src/js.cookie.js')
        .pipe(uglify())
        .pipe(gulp.dest("./deploy/"+type+"/")),

    //webix
    gulp.src('libs/webix/codebase/'+webix+'.js')
        .pipe(gulp.dest("./deploy/"+type+"/")),


        //index
    gulp.src("./index.html")
        .pipe(replace('data-main="app" src="libs/requirejs/require.js"', 'src="app.js"'))
        .pipe(replace('<script type="text/javascript" src="libs/less.min.js"></script>', ''))
        .pipe(replace('libs\/js-cookie\/src\/js.cookie.js', 'js.cookie.js'))
        .pipe(replace(/rel\=\"stylesheet\/less\" href=\"(.*?)\.less\"/g, 'rel="stylesheet" href="$1.css"'))
        .pipe(replace(/\.css\"/g, '.css?'+build+'"'))
        .pipe(replace(/\.js\"/g, '.js?'+build+'"'))
        .pipe(replace("require.config", "webix.production = true; require.config"))
        .pipe(replace(/libs\/webix\/codebase\//g, ''))
        .pipe(gulp.dest("./deploy/"+type+"/")),

        //server
    gulp.src(["./server/**/*.*",
              "!./server/*.log", "!./server/config.*",
              "!./server/dev/**/*.*", "!./server/dump/**/*.*"])
        .pipe(gulp.dest("./deploy/"+type+"/server/"))
    );

});


gulp.task('lint', function() {
    return gulp.src(['./views/**/*.js', './helpers/**/*.js', './models/**/*.js', './*.js', "!./jshint.conf.js"])
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

/*var optimizeImg = require('gulp-image');
gulp.task('optimage',function(){
    return gulp.src('./assets/imgs/*')
            .pipe(optimizeImg())
            .pipe(gulp.dest('./deploy/assets/imgs'));
})*/

